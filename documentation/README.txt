Equipe:
  - Fernanda Barros
  - Katharine Schramm

Função dos arquivos por diretório, dentro da pasta application/:
  1. classifier/ => arquivos de classificação
     1.1. preprocessor.rb => arquivo de pré-processamento
     1.2. classifier.rb => arquivo de classificação
     1.3. validation.rb => arquivo de validação
     1.4. Gemfile e Gemfile.lock => instalação das dependências (gemas)

  2. DATASET/ => base de dados
    2.1. raw_data/ => bases de dados antes do pré-processamento
       2.1.1. ham_collection/ => base de postagens legítimas
       2.1.2. spam_collection/ => base de spams
    2.2. preprocessed/ => base de dados depois do pré-processamento
       2.2.1. ham_collection/ => postagens legítimas
       2.2.2. spam_collection/ => base de spams

   3. utilities/
    3.1. stopwords.txt => coleção de palavras irrelevantes para o classificador
    3.2. classifier_snapshot.dat => snapshot do classificador


Instação (dentro da pasta application/):
  1. Instale o Ruby versão >= 2.3.3p22 
	   # sudo apt-get install ruby ruby-dev ruby-railties 

  2. Instale o Rails versão >= 5.2.1
	   # sudo gem install rails

  3. Instale o bundler
	   # sudo apt-get install ruby-bundler

  4. Na pasta /classifier, execute o comando:
	   # bundle install

Execução (dentro da pasta application/):
  Para executar o programa, acesse a pasta classifier/ e execute os comandos:
  1. Pré-processamento:
  	1.1. Para pré-processar a base de HAMs:
				 # ruby preprocessor.rb 
				 # 1
	  1.2. Para pré-processar a base de SPAMs:
	  		 # ruby preprocessor.rb
		  	 # 2
 	2. Classificação:
 		 # ruby classifier.rb
 	3. Validação:
 		 # ruby validation.rb
