require 'classifier-reborn'
include ClassifierReborn::ClassifierValidator

trained_classifier = Marshal.load File.read("../utilities/classifier_snapshot.dat")

spam_collection = Dir.glob("../DATASET/preprocessed/spam_collection/*")
ham_collection = Dir.glob("../DATASET/preprocessed/ham_collection/*")

sample_data = Array.new

for spam in spam_collection do
  arr_data = ["Spam", File.read(spam)]
  sample_data.push(arr_data)
end 

for ham in ham_collection do
  arr_data = ["Ham", File.read(ham)]
  sample_data.push(arr_data)
end
 
cross_validate(trained_classifier, sample_data, 5)
