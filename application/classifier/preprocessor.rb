require 'fileutils'
require 'action_view'
require 'i18n'
include ActionView::Helpers::SanitizeHelper 

# vetor de stopwords
stopwords = File.read("../utilities/stopwords.txt")
# retira os acentos do vetor de stopwords
stopwords = I18n.transliterate(stopwords).split

puts "Digite\n 1-Para pré-processar a base de HAM\n 2-Para pré-processar a base de SPAM"
input = gets.chomp

# encerra a execução em caso de entrada inválida
if(input != "1" && input != "2")
  exit(1)
end

# pré-processamento da base de HAM
if(input == "1")
	# diretorio para ler os arquivos
	files = Dir.glob("../DATASET/raw_data/ham_collection/*")
	# diretorio para salvar os textos pré-processados
	caminho_diretorio = "../DATASET/preprocessed/ham_collection"

# pré-processamento da base de SPAM
else
	# diretorio para ler os arquivos
	files = Dir.glob("../DATASET/raw_data/spam_collection/*")
	# diretorio para salvar os textos pré-processados
	caminho_diretorio = "../DATASET/preprocessed/spam_collection"
end

hash_post = Hash.new

for file in files do
  data = File.read(file)

  # Pre-processamento do conjunto de HAM 
  if(input == "1")			
    # remove URLs
		data = data.gsub(%r"(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)", '')

		# adiciona whitespaces entre as tags html
		data = data.gsub('>', '> ')
		data = data.gsub('<', ' <')

		# remove tags html
		data = strip_tags(data)
	end

	# remove acentos
	data = I18n.transliterate(data)

  # retira caracteres especiais e coloca tudo em letras minusculas 
  tokenizer = data.gsub(/[^a-zA-Zçs]/i, ' ').mb_chars.downcase

 	# tira espaços em branco, obtendo array
  tokenizer = tokenizer.strip.split(" ")

	# retira stopwords
	tokenizer = tokenizer.delete_if{|x| stopwords.include?(x)}
		
  # adiciona ao array de posts
  hash_post[File.basename(file, ".txt")] = tokenizer

end

# apaga o diretorio de salvar os arquivos caso ele ja exista
if(File.exists?(caminho_diretorio))
  FileUtils.remove_dir(caminho_diretorio,true)
end

Dir.mkdir(caminho_diretorio) 

# percorre o array de posts e salva o conteudo de cada posicao em um arquivo
hash_post.each do |key, post|
  caminho_arquivo = caminho_diretorio + '/' + key 

	# adiciona espaços entre as palavras
  conteudo = post.join(" ")

  File.write(caminho_arquivo, conteudo)

end

puts "Done"
