require 'classifier-reborn'

classifier = ClassifierReborn::Bayes.new(
	"Spam","Ham"            # categorias
)

spam_dataset = Dir.glob("../DATASET/preprocessed/spam_collection/*")
ham_dataset = Dir.glob("../DATASET/preprocessed/ham_collection/*")

# treina o classificador com a base de spams pré-processados
for spam in spam_dataset do
  data = File.read(spam)
	classifier.train "Spam", data
end

# treina o classificador com a base de hams pré-processados
for ham in ham_dataset do
  data = File.read(ham)
	classifier.train "Ham", data
end

# faz um snapshot do classificador
classifier_snapshot = Marshal.dump classifier
File.open("../utilities/classifier_snapshot.dat", "w") {|f| f.write(classifier_snapshot) }
